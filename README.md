# PiHole

Create blacklist site list to filter adservers using some public available sources in internet:

* https://adaway.org/hosts.txt
* http://adblock.gjtech.net/?format=unix-hosts
* http://hosts-file.net/.%5Cad_servers.txt
* http://www.malwaredomainlist.com/hostslist/hosts.txt
* https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext
* http://someonewhocares.org/hosts/hosts
* http://winhelp2002.mvps.org/hosts.txt

Use resulting file `gravity.list` to block sites appending it to your `/etc/hosts` file.

Additional sources if you are interested: https://github.com/jmdugan/blocklists

P.S.: I found the original script somewhere but don't remember where, when or who did it first. If you know notify me.
