#!/bin/bash
# http://pi-hole.net
# Compiles a list of ad-serving domains by downloading them from multiple sources

# This script should only be run after you have a static IP address set on the Pi
piholeIP=$(hostname -I)

# Ad-list sources--one per line in single quotes
sources=('https://adaway.org/hosts.txt'
'http://adblock.gjtech.net/?format=unix-hosts'
'http://hosts-file.net/.%5Cad_servers.txt'
'http://www.malwaredomainlist.com/hostslist/hosts.txt'
'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext'
'http://someonewhocares.org/hosts/hosts'
'http://winhelp2002.mvps.org/hosts.txt')

# Variables for various stages of downloading and formatting the list
adList=./gravity.list
origin=./
piholeDir=./
justDomainsExtension=domains
matter=pihole.0.matter.txt
andLight=pihole.1.andLight.txt
supernova=pihole.2.supernova.txt
eventHorizon=pihole.3.eventHorizon.txt
accretionDisc=pihole.4.accretionDisc.txt
eyeOfTheNeedle=pihole.5.wormhole.txt
blacklist=$piholeDir/blacklist.txt
latentBlacklist=$origin/latentBlacklist.txt
latentWhitelist=$origin/latentWhitelist.txt

# After setting defaults, check if there's local overrides
if [[ -r $piholeDir/pihole.conf ]];then
    echo "** Local calibration requested..."
	. $piholeDir/pihole.conf
fi


echo "** Neutrino emissions detected..."

# Loop through domain list.  Download each one and remove commented lines (lines beginning with '# 'or '/') and blank lines
for ((i = 0; i < "${#sources[@]}"; i++))
do
	url=${sources[$i]}
	# Get just the domain from the URL
	domain=$(echo "$url" | cut -d'/' -f3)

	# Save the file as list.#.domain
	saveLocation=$origin/list.$i.$domain.$justDomainsExtension

		echo -n "Getting $domain list... "
	# Use a case statement to download lists that need special cURL commands to complete properly
	case "$domain" in
		"adblock.mahakala.is") data=$(curl -s -A 'Mozilla/5.0 (X11; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0' -e http://forum.xda-developers.com/ -z $saveLocation $url);;

		"pgl.yoyo.org") data=$(curl -s -d mimetype=plaintext -d hostformat=hosts -z $saveLocation $url);;

		*) data=$(curl -s -z $saveLocation -A "Mozilla/10.0" $url);;
	esac

	if [[ -n "$data" ]];then
		# Remove comments and print only the domain name
		# Most of the lists downloaded are already in hosts file format but the spacing/formating is not contigious
		# This helps with that and makes it easier to read
		# It also helps with debugging so each stage of the script can be researched more in depth
		echo "$data" | awk 'NF {if ($1 !~ "#") { if (NF>1) {print $2} else {print $1}}}' | \
			sed -e 's/^[. \t]*//' -e 's/\.\.\+/./g' -e 's/[. \t]*$//' | grep "\." > $saveLocation
		echo "Done."
	else
		echo "Skipping list because it does not have any new entries."
	fi
done

# Find all files with the .domains extension and compile them into one file and remove CRs
echo "** Aggregating list of domains..."
find $origin/ -type f -name "*.$justDomainsExtension" -exec cat {} \; | tr -d '\r' > $origin/$matter

# Append blacklist entries if they exist
if [[ -f $blacklist ]];then
	numberOf=$(cat $blacklist | sed '/^\s*$/d' | wc -l)
	echo "** Blacklisting $numberOf domain(s)..."
	cat $blacklist >> $origin/$matter
else
	:
fi

function gravity_advanced()
###########################
	{
	numberOf=$(cat $origin/$andLight | sed '/^\s*$/d' | wc -l)
	echo "** $numberOf domains being pulled in by gravity..."
	# Remove carriage returns and preceding whitespace
	cat $origin/$andLight | sed $'s/\r$//' | sed '/^\s*$/d' > $origin/$supernova
	# Sort and remove duplicates
	cat $origin/$supernova | sort | uniq > $origin/$eventHorizon
	numberOf=$(cat $origin/$eventHorizon | sed '/^\s*$/d' | wc -l)
	echo "** $numberOf unique domains trapped in the event horizon."
	# Format domain list as "192.168.x.x domain.com"
	echo "** Formatting domains into a HOSTS file..."
	cat $origin/$eventHorizon | awk '{sub(/\r$/,""); print "127.0.0.1 '"$piholeIP"'" $0}' > $origin/$accretionDisc
    echo "Copy the file over as /etc/pihole/gravity.list so dnsmasq can use it"
	sudo cp $origin/$accretionDisc $adList
	}

  # Prevent our sources from being pulled into the hole
  plural=; [[ "${#sources[@]}" != "1" ]] && plural=s
  echo "** Whitelisting ${#sources[@]} ad list source${plural}..."
  for url in ${sources[@]}
  do
  	echo "$url" | awk -F '/' '{print "^"$3"$"}' | sed 's/\./\\./g' >> $latentWhitelist
  done

  grep -vxf $latentWhitelist $origin/$matter > $origin/$andLight

gravity_advanced
